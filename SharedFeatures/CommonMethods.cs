﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;

namespace Kiosk.StackOverFlow.SharedFeatures
{
    public class CommonMethods
    {
        public static string GetAppSettingsValue(string Key)
        {
            string value = null;
            try
            {
                value = ConfigurationManager.AppSettings[Key].ToString();
            }

            catch (Exception ee)
            {
                value = null;
            }

            return value;
        }

        public static string GetWebRequestString(string url, WebClient client)
        {
            using (client)
            {
                return client.DownloadString(url);
            }
        }

    }
}
