﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kiosk.StackOverFlow.Model;
using Newtonsoft.Json;
using System.Net;
using System.Web;
using Kiosk.StackOverFlow.SharedFeatures;
using Kiosk.StackOverFlow.Data.Questions;

namespace Kiosk.StackOverFlow.Business.Questions
{
    public class QuestionService
    {
                
        private IQuestionRepository _QuestionRepository;
        public QuestionService(string QuestionsUrl, string QuestionUrl)
        {            
            _QuestionRepository = new QuestionRepository(QuestionsUrl, QuestionUrl);
        }
        public QuestionDTO GetQuestionById(int id)
        {
            QuestionDTO result = null;
            try
            {
               result =  _QuestionRepository.GetQuestionById(id);
            }
            catch (Exception exeption)
            {
                //Logger.Log(exception)     //log the exception
            }

            return result;
        }

        public List<QuestionDTO> GetQuestionsList(int pageIndex, int pageSize)
        {
            List<QuestionDTO> result = null;

            try
            {
                result = _QuestionRepository.GetQuestionsList(pageIndex, pageSize);
            }
            catch (Exception exeption)
            {

                //Logger.Log(exception)     //log the exception
            }

            return result;
        }

    }
}
