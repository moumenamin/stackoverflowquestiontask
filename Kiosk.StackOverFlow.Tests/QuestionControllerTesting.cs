﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Kiosk.StackOverFlowTask.Controllers;
using Kiosk.StackOverFlowTask.Models;
using AutoMapper;
using Kiosk.StackOverFlow.Model;

namespace Kiosk.StackOverFlow.Tests
{
    [TestClass]
    public class QuestionControllerTest
    {
        static QuestionControllerTest()
        {
            InitAutoMapper();
        }
        [TestMethod]
        public void Index()
        {
            QuestionController controller = new QuestionController();

            // Act
            ViewResult result = controller.Index(1) as ViewResult;
            var model = result.Model as PagedList.IPagedList<QuestionVM>;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(50, model.Count);
        }

        [TestMethod]
        public void Details()
        {
            QuestionController controller = new QuestionController();
            // Act
            ViewResult result = controller.Details(47370152) as ViewResult;
            var model = result.Model as QuestionVM;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(47370152, model.Question_Id);
        }
        private static void InitAutoMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<QuestionDTO, QuestionVM>()
                .ForMember(q => q.Tags, src => src.MapFrom<string>(s => string.Join(",", s.Tags)))
                .ForMember(q => q.Creation_Date, src => src.MapFrom<DateTime>(s => s.Creation_Date.ToLocalTime()))
                .ForMember(q => q.Last_Activity_Date, src => src.MapFrom<DateTime>(s => s.Last_Activity_Date.ToLocalTime()));
            });
        }
    }
}
