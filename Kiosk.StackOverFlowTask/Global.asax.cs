﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using Kiosk.StackOverFlow.Model;
namespace Kiosk.StackOverFlowTask
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
          
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<QuestionDTO, Models.QuestionVM>()
                .ForMember(q => q.Tags, src => src.MapFrom<string>(s => string.Join(",", s.Tags)))
                .ForMember(q => q.Creation_Date, src => src.MapFrom<DateTime>(s => s.Creation_Date.ToLocalTime()))
                .ForMember(q => q.Last_Activity_Date, src => src.MapFrom<DateTime>(s => s.Last_Activity_Date.ToLocalTime()));
            });
        }
    }
}
