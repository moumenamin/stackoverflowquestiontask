﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Kiosk.StackOverFlowTask.Models
{
    public class QuestionVM
    {   
        [Display(Name = "Question Id")]
        public int Question_Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }       
        [Display(Name = "Creation Date")]
        public DateTime Creation_Date { get; set; }
        [Display(Name = "Last Activity Date")]
        public DateTime Last_Activity_Date { get; set; }
        public float Score { get; set; }
        public String Tags { get; set; }
    }
}