﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kiosk.StackOverFlow.Model;
using PagedList;
using AutoMapper;
using Kiosk.StackOverFlowTask.Models;
using Kiosk.StackOverFlow.SharedFeatures;
using Kiosk.StackOverFlow.Business.Questions;

namespace Kiosk.StackOverFlowTask.Controllers
{
    public class QuestionController : Controller
    {
        private string _QuestionsUrl;
        private string _QuestionUrl;
        public QuestionController()
        {
             _QuestionsUrl = HttpUtility.HtmlDecode(CommonMethods.GetAppSettingsValue("StackOverFlowApiQuestionsListUrl"));
             _QuestionUrl = HttpUtility.HtmlDecode(CommonMethods.GetAppSettingsValue("StackOverFlowApiQuestionByIdUrl"));            
        }
        // GET: Question
        public ActionResult Index(int? page)
        {
            var pageIndex = page.HasValue ? page.Value : 1;
            var pageSize = Convert.ToInt32(CommonMethods.GetAppSettingsValue(Strings.PageSize));

            QuestionService service = new QuestionService(_QuestionsUrl, _QuestionUrl);
            var questionsDto = service.GetQuestionsList(pageIndex ,100);
            var questionsVM = Mapper.Map<List<QuestionDTO>, List<QuestionVM>>(questionsDto);

            return View(questionsVM.ToPagedList(pageIndex, pageSize));
        }

        public ActionResult Details(int Id)
        {
            QuestionService service = new QuestionService(_QuestionsUrl, _QuestionUrl);
            var questionDetailsDto = service.GetQuestionById(Id);
            var questionDetailsVM = Mapper.Map<QuestionDTO, Models.QuestionVM>(questionDetailsDto);

            return View(questionDetailsVM);
        }
    }
}