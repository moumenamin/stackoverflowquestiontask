﻿using Kiosk.StackOverFlow.SharedFeatures;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kiosk.StackOverFlow.Model
{
    public class QuestionDTO
    {
        public int Question_Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        [JsonConverter(typeof(MicrosecondEpochConverter))]
        public DateTime Creation_Date { get; set; }
        [JsonConverter(typeof(MicrosecondEpochConverter))]
        public DateTime Last_Activity_Date { get; set; }
        public float Score { get; set; }
        public List<string> Tags { get; set; }

    }
    public class RootQuestions
    {
       public List<QuestionDTO> items { set; get; }
    }

    
}
