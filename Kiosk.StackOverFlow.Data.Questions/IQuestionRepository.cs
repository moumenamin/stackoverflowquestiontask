﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kiosk.StackOverFlow.Model;

namespace Kiosk.StackOverFlow.Data.Questions
{
    public interface IQuestionRepository
    {
        List<QuestionDTO> GetQuestionsList(int page, int pageSize);

        QuestionDTO GetQuestionById(int id);
    }
}
