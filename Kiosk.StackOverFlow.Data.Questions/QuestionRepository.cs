﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kiosk.StackOverFlow.Model;
using Newtonsoft.Json;
using System.Net;
using System.Web;
using Kiosk.StackOverFlow.SharedFeatures;
namespace Kiosk.StackOverFlow.Data.Questions
{
    public class QuestionRepository : IQuestionRepository
    {
        private string _QuestionsUrl;
        private string _QuestionUrl;

        public QuestionRepository(string QuestionsUrl, string QuestionUrl)
        {
            _QuestionsUrl = QuestionsUrl;
            _QuestionUrl = QuestionUrl;
        }
        public QuestionDTO GetQuestionById(int id)
        {
            QuestionDTO result = null;

            try
            {
                var url = string.Format(_QuestionUrl, id);

                using (var gZipClient = new GZipWebClient())
                {
                    var questionsString = CommonMethods.GetWebRequestString(url, gZipClient);
                    var questions = JsonConvert.DeserializeObject<RootQuestions>(questionsString);

                    if (questions != null && questions.items.Count > 0)
                    {
                        result = questions.items.First();
                    }
                }

            }
            catch (Exception exeption)
            {
                //Logger.Log(exception)     //log the exception
            }

            return result;
        }

        public List<QuestionDTO> GetQuestionsList(int pageIndex, int pageSize)
        {
            List<QuestionDTO> result = null;

            try
            {
                var url = string.Format(_QuestionsUrl, pageIndex, pageSize);

                using (var gZipWebClient = new GZipWebClient())
                {
                    var questionsString = CommonMethods.GetWebRequestString(url, gZipWebClient);
                    var questions = JsonConvert.DeserializeObject<RootQuestions>(questionsString);

                    result = questions.items;
                }

            }
            catch (Exception exeption)
            {

                //Logger.Log(exception)     //log the exception
            }

            return result;
        }

    }
}
