﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kiosk.StackOverFlowTask;
using Kiosk.StackOverFlowTask.Controllers;
using Kiosk.StackOverFlow.Business;
using Kiosk.StackOverFlowTask.Models;
using AutoMapper;
using Kiosk.StackOverFlow.Model;

namespace Kiosk.StackOverFlowTask.Tests.Controllers
{
    [TestClass]
    public class QuestionControllerTest
    {
        void InitAutoMapper()
        {
            Mapper.Initialize(cfg =>
                {
                    cfg.CreateMap<QuestionDTO, Models.QuestionVM>()
                    .ForMember(q => q.Tags, src => src.MapFrom<string>(s => string.Join(",", s.Tags)))
                    .ForMember(q => q.Creation_Date, src => src.MapFrom<DateTime>(s => s.Creation_Date.ToLocalTime()))
                    .ForMember(q => q.Last_Activity_Date, src => src.MapFrom<DateTime>(s => s.Last_Activity_Date.ToLocalTime()));
                });
        }
        [TestMethod]
        public void Index()
        {
            // Arrange
            
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
